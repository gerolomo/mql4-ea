//+------------------------------------------------------------------+
//|                                               TARS Indicator.mq4 |
//|                                                  Felipe Gerolomo |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Felipe Gerolomo"
#property link "https://www.mql5.com"
#property version "1.00"
#property indicator_chart_window
#property indicator_buffers 2
// #property indicator_color1 Tomato
// #property indicator_color2 DeepSkyBlue

extern int minChannel = 100;
extern int maxChannel = 110;
extern int qtdCandle = 15;

// -- buffers

int currentBars;
int currentChannelCandles;
double currentChannel;
double highCandle;
double lowCandle;
double resistance;
double support;
datetime highCandleTime;
datetime lowCandleTime;
bool isCanal = false;

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int init()
  {
   return (0);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
int start()
  {

   datetime currentDate = TimeCurrent();
   datetime lastWeek = iTime(NULL, PERIOD_W1, 1);

   currentBars = Bars(NULL, 0, currentDate, 1576547817);

   while(currentBars >= 0)
     {
      resistance = iFractals(NULL, 0, MODE_UPPER, currentBars);
      support = iFractals(NULL, 0, MODE_LOWER, currentBars);

      if(resistance > 0 && support == 0)
        {
         currentChannel = High[currentBars];
         highCandle = High[currentBars];
         highCandleTime = iTime(0, 0, currentBars);
        }

      if(support > 0 && resistance == 0)
        {

         currentChannel = MathAbs((currentChannel - Low[currentBars]) / _Point);
         currentChannelCandles = Bars(NULL, 0, Time[currentBars], highCandleTime);

         if(currentChannel >= minChannel && currentChannel <= maxChannel && isCanal == false)
           {
            isCanal = true;
            Print("TAMANHO DO CANAL ATUAL = " + currentChannel + " QUANTIDADE DE CANDLES ATUAL = " + currentChannelCandles);
            lowCandleTime = iTime(0, 0, currentBars);
            lowCandle = Low[currentBars];

            drawLine("SUPPORT", lowCandle, C'192,57,43');
            drawArrow("SUPPORT_ARROW", lowCandleTime, lowCandle, SYMBOL_ARROWUP,  C'192,57,43');
            drawLine("RESISTENCE", highCandle, C'192,57,43');
            drawArrow("RESISTENCE_ARROW", highCandleTime, highCandle+20*Point, SYMBOL_ARROWDOWN,  C'41,128,185');
            drawChannelsTop("SUPPORT_", lowCandle, 7);
            drawChannelsBottom("RESISTENCE_", lowCandle, 7);
           }
        }

      currentBars--;
     }
   return (0);
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawLine(string name, double price, color lineColor)
  {
   ObjectCreate(name, OBJ_HLINE, 0, 0, price);
   ObjectSet(name, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSet(name, OBJPROP_COLOR, lineColor);
   ObjectSet(name, OBJPROP_WIDTH, 2);
   return;
  }

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
void drawArrow(string name, datetime time, double price, int arrowType, color lineColor)
  {
   ObjectCreate(name, OBJ_ARROW, 0, time, price); //draw an up arrow
   ObjectSet(name, OBJPROP_STYLE, STYLE_SOLID);
   ObjectSet(name, OBJPROP_ARROWCODE, arrowType);
   ObjectSet(name, OBJPROP_COLOR, lineColor);
   ObjectSet(name, OBJPROP_WIDTH, 5);
   return;
  }

//+------------------------------------------------------------------+
//+------------------------------------------------------------------+
void drawChannelsTop(string name, double price, int qtdChannel)
  {
   price = price + (currentChannel * Point);
   for(int y = 0; y < qtdChannel; y++)
     {
      price = price + (currentChannel * Point);
      drawLine(name + y, price, C'39,174,96');
     }
   return;
  }
//+------------------------------------------------------------------+
void drawChannelsBottom(string name, double price, int qtdChannel)
  {
   for(int y = 0; y < qtdChannel; y++)
     {
      price = price - (currentChannel * Point);
      drawLine(name + y, price, C'39,174,96');
     }
   return;
  }
//+------------------------------------------------------------------+
